/*
return gulp.src('app/styles/*.scss')
.pipe($.plumber())
.pipe($.sourcemaps.init())
.pipe($.sass.sync({
outputStyle: 'expanded',
precision: 10,
includePaths: ['.']
}).on('error', $.sass.logError))
.pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
.pipe($.sourcemaps.write())
.pipe(gulp.dest('.tmp/styles'))
.pipe(gulpHash())
.pipe(gulp.dest('.tmp/styles'))
.pipe(gulpHash.manifest('assets.json'))
.pipe(gulp.dest('.tmp'))
.pipe(reload({stream: true}));*/



JS

return gulp.src(['app/scripts/models/*.js', 'app/scripts/collections/*.js', 'app/scripts/controllers/*.js'])
.pipe($.concat('app.js'))
.pipe($.plumber())
.pipe($.sourcemaps.init())
.pipe($.babel())
.pipe($.sourcemaps.write('.'))
.pipe(gulp.dest('.tmp/scripts'))
.pipe(gulp.dest('dist/scripts'))
.pipe(reload({stream: true}));
