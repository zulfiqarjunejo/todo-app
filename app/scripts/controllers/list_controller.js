class ListController {

  constructor() {
    this.self = this;
    this.collection = new ListCollection();
    this.initView();
    this.bind(this.self);
    this.updateView();
  }

  changeMode(mode) {
    this.mode = mode;
    this.updateView();
  }

  initView() {
    var source = $('#todo-app-template').html();
    this.template = Handlebars.compile(source);
    this.mode = 'All';
  }

  bind(self) {

    $('#todo-app').on('click', '#input-toggle-items', function() {
      self.toggleItems(this.getAttribute('toggle-status'));
    });

    $('#todo-app').on('keypress', '#input-text-item', function(e) {
      if(e.which == 13)
        self.addItem();
    });

    $('#todo-app').on('click', '.item-toggle', function() {
      self.toggle(this.getAttribute('data-item-id'));
    });

    $('#todo-app').on('dblclick', '.item', function() {
      this.removeAttribute('readonly');
    });

    $('#todo-app').on('keypress', '.item', function(e) {
      if(e.which == 13)
        self.editItem(this.getAttribute('data-item-id'), this.value);
    });

    $('#todo-app').on('mouseleave', '.item', function(e) {
      self.editItem(this.getAttribute('data-item-id'), this.value);
    });

    $('#todo-app').on('click', '.item-remove', function() {
      self.removeItem(this.getAttribute('data-item-id'));
    });

    $('#todo-app').on('click','#link_showAll', function() {
      self.changeMode('All');
    });

    $('#todo-app').on('click', '#link_showCompleted', function() {
      self.changeMode('Completed');
    });

    $('#todo-app').on('click', '#link_showActive', function() {
      self.changeMode('Active');
    });

    $('#todo-app').on('click', '#link_clearCompleted', function() {
      self.removeCompleted();
    });
  }

  updateView() {
    var data,
        countCompleted,
        countLeft,
        isModeAll = false,
        isModeActive = false,
        isModeCompleted = false,
        isAnyItemComplete = false,
        isAnyItemLeft = false;
    if(this.mode == 'All')
      data = this.collection.getAllItems();
    else if (this.mode == 'Active')
      data = this.collection.getActiveItems();
    else
      data = this.collection.getCompleteItems();
    countCompleted = this.collection.getCountCompleted();
    countLeft = this.collection.getCountLeft();
    if (this.mode == 'All')
      isModeAll = true;
    else if (this.mode == 'Active')
      isModeActive = true;
    else
      isModeCompleted = true;
    if(countLeft > 0)
      isAnyItemLeft = true;
    if(countCompleted > 0)
      isAnyItemComplete = true;
    $('#todo-app').html(this.template({
      items: data,
      countCompleted : countCompleted,
      countLeft : countLeft,
      isModeAll : isModeAll,
      isModeActive : isModeActive,
      isModeCompleted : isModeCompleted,
      isAnyItemLeft : isAnyItemLeft,
      isAnyItemComplete : isAnyItemComplete
    }));
    $('#input-text-item').val('');
    $('#input-text-item').focus();
  }

  addItem() {
    var text = $('#input-text-item').val();
    console.log('NEW TODO -->> Text: ' , text);
    if(text != '') {
      this.collection.addItem(text);
    }
    this.updateView();
  }

  toggle(id) {
    this.collection.toggle(id);
    this.updateView();
  }

  toggleItems(status) {
    if(status == 'active')
      this.collection.toggleItems(true);
    else
     this.collection.toggleItems(false);
     this.updateView();
  }

  editItem(id, text) {
    this.collection.editItem(id, text);
    this.updateView();
  }

  removeItem(id) {
    this.collection.removeItem(id);
    this.updateView();
  }

  removeAllItems() {
  }

  removeCompleted() {
    this.collection.removeCompletedItems();
    this.updateView();
  }
}
