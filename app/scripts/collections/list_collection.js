class ListCollection {
  constructor() {
    this.restoreData();
  }

  storeData() {
    if(typeof(Storage) !== 'undefined') {
      localStorage.setItem('items', JSON.stringify(this.items));
      localStorage.setItem('count', this.count);
    }
  }

  restoreData() {
    if(typeof(Storage) !== 'undefined') {
      if (localStorage['items']) {
        this.items = JSON.parse(localStorage.getItem('items'));
        this.count = Number(localStorage.getItem('count'));
      } else {
        this.items = [];
        this.count = 0;
      }
    } else {
      alert('Data will be cleared after session.');
    }
  }



  createItem(text) {
    var item = new Item(this.count, text, false);
    this.count++;
    return item;
  }

  addItem(text) {
    this.items.push(this.createItem(text));
    this.storeData();
  }

  getAllItems() {
    return [].concat(this.items);
  }

  getActiveItems() {
    return [].concat(this.items.filter( function(item) {
      return !item.isCompleted;
    }));
  }

  getCompleteItems() {
    return [].concat(this.items.filter(function(item) {
      return item.isCompleted;
    }));
  }

  getCountCompleted() {
    var count = 0;
    for (var i = 0; i < this.items.length; i++) {
      if(this.items[i].isCompleted)
        count++;
    }
    return count;
  }

  getCountLeft() {
    var count = 0;
    for (var i = 0; i < this.items.length; i++) {
      if(!this.items[i].isCompleted)
        count++;
    }
    return count;
  }

  toggle(id) {
    var index;
    for(var i=0; i<this.items.length; i++) {
      if(id == this.items[i].id) {
        index = i;
      }
    }
    if(this.items[index].isCompleted)
      this.items[index].isCompleted = false;
    else
      this.items[index].isCompleted = true;

    this.storeData();
  }

  toggleItems(status) {
    for(var i=0; i<this.items.length; i++) {
      this.items[i].isCompleted = status;
    }

    this.storeData();
  }

  editItem(id, text) {
    var index;
    for(var i=0; i<this.items.length; i++) {
      if(id == this.items[i].id) {
        index = i;
      }
    }
    this.items[index].text = text;

    this.storeData();
  }

  removeItem(id) {
    var index;
    for(var i=0; i<this.items.length; i++) {
      if(id == this.items[i].id) {
        index = i;
      }
    }
    this.items.splice(index, 1);

    this.storeData();
  }

  removeAllItems() {
    this.items = [];

    this.storeData();
  }

  removeCompletedItems() {
    this.items = this.items.filter(function(item) {
      return !item.isCompleted;
    });

    this.storeData();
  }
}
