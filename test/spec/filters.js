describe('Test Phase todo-app | Filter Tasks - ', function() {

  var TodoApp = require('./task/TodoApp');
  var todoApp = new TodoApp();

  browser.ignoreSynchronization = true;
  todoApp.get();

  it("Check All Items | Should Contain 3 Items", function() {
    todoApp.showAllItems();
    expect(todoApp.getItemListLength()).toBe(3);
  });

  it("Check Active Items | Should Contain 2 Items", function() {
    todoApp.showActiveItems();
    expect(todoApp.getItemListLength()).toBe(2);
  });

  it("Check Complete Items | Should Contain 1 Items", function() {
    todoApp.showCompletedItems();
    expect(todoApp.getItemListLength()).toBe(1);
  });

});
