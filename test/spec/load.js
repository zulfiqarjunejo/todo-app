describe('Training: todo-app end2end testing | Load Application', function() {
  browser.ignoreSynchronization = true;
  browser.get('localhost:9000/');

  it("Todo Application Should Load Successfully", function() {
    expect(browser.getTitle()).toEqual('todoapp');
    expect(element(by.css('h1')).getText()).toEqual('todos');
  });

  it('Todo list should be empty initially', function() {
    expect(element.all(by.css('.item-group')).count()).toBe(0);
  });

});
