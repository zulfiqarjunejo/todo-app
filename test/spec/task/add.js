describe('Test Phase todo-app | Add Tasks - ', function() {
  var TodoApp = require('./todoApp');
  var todoApp = new TodoApp();

  browser.ignoreSynchronization = true;
  todoApp.get();

  it('Add item # 1.', function() {
    todoApp.setInputText('Become a super saiyan!');
    expect(todoApp.getTodoText(0)).toBe('Become a super saiyan!');
    expect(todoApp.getItemListLength()).toBe(1);
  });

  it('Add item # 2. Should add successfully with id incremented by one.', function() {
    todoApp.setInputText('Create test cases :D');
    expect(todoApp.getTodoText(1)).toBe('Create test cases :D');
    expect(todoApp.getItemListLength()).toBe(2);
  });

  it('Add item # 3. Should add successfully with id incremented by one.', function() {
    todoApp.setInputText('Meet CR7 someday InshaAllah');
    expect(todoApp.getTodoText(2)).toBe('Meet CR7 someday InshaAllah');
    expect(todoApp.getItemListLength()).toBe(3);
  });

});
