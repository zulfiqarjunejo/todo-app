module.exports = class TodoApp {

  constructor() {
    this.inputNewTodo = element(by.id('input-text-item'));
    this.inputToggleItem = element(by.id('input-toggle-items'));
  }

  temp() {
    return 'WWW.WORKING.COM';
  }

  get() {
    browser.get('localhost:9000/');
  }

  getInputText() {
    return this.inputNewTodo.getAttribute('value');
  }

  setInputText(text) {
    return this.inputNewTodo.sendKeys(text, protractor.Key.ENTER);
  }

  getTodoText(id) {
    return element(by.css('.input-group .form-control.item[data-item-id = "' + id + '"]')).getAttribute('value');
  }

  setTodoText(id, text) {
    browser.actions().mouseMove(element(by.css('.input-group .form-control.item[data-item-id = "' + id + '"]'))).doubleClick().perform();
    element(by.css('.input-group .form-control.item[data-item-id = "' + id + '"]')).clear();
    element(by.css('.input-group .form-control.item[data-item-id = "' + id + '"]')).sendKeys(text, protractor.Key.ENTER);
  }

  getTodoState(id) {
    var value = element(by.css('.input-group .input-group-addon.item-toggle[data-item-id = "' + id + '"]')).getAttribute('class');
    if(value == 'input-group-addon item-toggle item-active')
      return 'Active';
    return 'Completed';
    //return value;
  }

  toggleTodoState(id) {
    //browser.actions().mouseMove(element(by.css('.input-group .input-group-addon.item-toggle[data-item-id = "' + id + '"]'))).click().perform();
    browser.actions().click(element(by.css('.input-group .input-group-addon.item-toggle[data-item-id = "' + id + '"]'))).perform();
  }

  getItemListLength() {
    return element.all(by.css('.item-group')).count();
  }

  removeTodo(id) {
    browser.actions().mouseMove(element(by.css('.input-group .form-control.item[data-item-id = "' + id + '"]'))).click(element(by.css('.input-group .input-group-addon.item-remove[data-item-id = "'+ id + '"]'))).perform();
  }

  isTodoPresent(id) {
    return element(by.css('.input-group .form-control.item[data-item-id = "' + id + '"]')).isPresent();
  }

  showAllItems() {
    browser.actions().click(element(by.id('link_showAll'))).perform();
  }

  showActiveItems() {
    browser.actions().click(element(by.id('link_showActive'))).perform();
  }

  showCompletedItems() {
    browser.actions().click(element(by.id('link_showCompleted'))).perform();
  }
}
