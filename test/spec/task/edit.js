describe('Test Phase todo-app | Edit Tasks - ', function() {

  var TodoApp = require('./TodoApp');
  var todoApp = new TodoApp();

  browser.ignoreSynchronization = true;
  todoApp.get();

  it('Change text of item # 2', function() {
    todoApp.setTodoText(1, 'A random wish');
    expect(todoApp.getTodoText(1)).toBe('A random wish');
  });

  it('Change state of item # 3', function() {
    todoApp.toggleTodoState(2);
    expect(todoApp.getTodoState(2)).toBe('Completed');
    //browser.pause();
  });

});
