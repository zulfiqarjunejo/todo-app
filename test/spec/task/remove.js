describe('Test Phase todo-app | Remove Tasks - ', function() {

  var TodoApp = require('./TodoApp');
  var todoApp = new TodoApp();

  browser.ignoreSynchronization = true;
  todoApp.get();

  it('Remove Task # 2', function() {
    todoApp.removeTodo(1);
    expect(todoApp.isTodoPresent(2)).toBe(false);
  });

});
